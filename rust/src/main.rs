mod blocks;

use blocks::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

#[derive(Debug)]
struct Question1 {
    dependencies: Vec<RcRefCellQuestion>,
}

impl Question for Question1 {
    fn name(&self) -> String {
        String::from("q1")
    }

    fn label(&self) -> String {
        String::from("Quesiton 1")
    }

    fn renderer(&self) -> BlockRenderer {
        BlockRenderer::CHECKBOX
    }

    fn default_answer(&self) -> BlockAnswer {
        BlockAnswer::BOOL(false)
    }

    fn answer_validator(&self) -> Box<dyn BlockAnswerValidator> {
        Box::new(BoolValidator {})
    }

    fn dependencies(&self) -> &Vec<RcRefCellQuestion> {
        &self.dependencies
    }
}

impl Clone for Question1 {
    fn clone(&self) -> Self {
        Question1 {
            dependencies: vec![],
        }
    }
}

#[derive(Debug)]
struct Question2 {
    dependencies: Vec<RcRefCellQuestion>,
}

impl Question for Question2 {
    fn name(&self) -> String {
        String::from("q2")
    }

    fn label(&self) -> String {
        String::from("Quesiton 2")
    }

    fn renderer(&self) -> BlockRenderer {
        BlockRenderer::INT
    }

    fn default_answer(&self) -> BlockAnswer {
        BlockAnswer::INT64(0)
    }

    fn answer_validator(&self) -> Box<dyn BlockAnswerValidator> {
        Box::new(Int64Validator {
            min: std::i64::MIN,
            max: std::i64::MAX,
        })
    }

    fn dependencies(&self) -> &Vec<RcRefCellQuestion> {
        &self.dependencies
    }
}

impl Clone for Question2 {
    fn clone(&self) -> Self {
        Question2 {
            dependencies: vec![],
        }
    }
}

#[derive(Debug)]
struct Question3 {
    dependencies: Vec<RcRefCellQuestion>,
}

impl Question for Question3 {
    fn name(&self) -> String {
        String::from("q3")
    }

    fn label(&self) -> String {
        String::from("Quesiton 3")
    }

    fn renderer(&self) -> BlockRenderer {
        BlockRenderer::INT
    }

    fn default_answer(&self) -> BlockAnswer {
        BlockAnswer::INT64(0)
    }

    fn answer_validator(&self) -> Box<dyn BlockAnswerValidator> {
        Box::new(Int64Validator {
            min: std::i64::MIN,
            max: std::i64::MAX,
        })
    }

    fn dependencies(&self) -> &Vec<RcRefCellQuestion> {
        &self.dependencies
    }
}

impl Clone for Question3 {
    fn clone(&self) -> Self {
        Question3 {
            dependencies: vec![],
        }
    }
}

fn main() {
    let mut motor = Motor::new(String::from("c1"));
    let mut q1 = Rc::new(RefCell::new(Question1 {
        dependencies: vec![],
    }));
    let mut q2 = Rc::new(RefCell::new(Question2 {
        dependencies: vec![],
    }));
    let mut q3 = Rc::new(RefCell::new(Question3 {
        dependencies: vec![],
    }));

    q2.borrow_mut().dependencies.push(q3.clone());
    q3.borrow_mut().dependencies.push(q1.clone());

    motor.push_dependency(q1);
    motor.push_dependency(q2);
    motor.push_dependency(q3);


    let answers = HashMap::new();

    motor.run(answers);

}
