use petgraph::algo::toposort;
use petgraph::graph::{DiGraph, NodeIndex};
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

#[derive(Debug, Clone)]
pub enum BlockAnswer {
    INT64(i64),
    FLOAT64(f64),
    STR(String),
    BOOL(bool),
    NONE,
}

pub trait BlockAnswerValidator {
    fn valid(&self, value: BlockAnswer) -> bool;
}

pub struct Int64Validator {
    pub min: i64,
    pub max: i64,
}

impl BlockAnswerValidator for Int64Validator {
    fn valid(&self, answer: BlockAnswer) -> bool {
        match answer {
            BlockAnswer::INT64(x) => true,
            _ => false,
        }
    }
}

pub struct BoolValidator;

impl BlockAnswerValidator for BoolValidator {
    fn valid(&self, answer: BlockAnswer) -> bool {
        match answer {
            BlockAnswer::BOOL(x) => true,
            _ => false,
        }
    }
}

pub struct NoneValidator;

impl BlockAnswerValidator for NoneValidator {
    fn valid(&self, answer: BlockAnswer) -> bool {
        match answer {
            BlockAnswer::NONE => true,
            _ => false,
        }
    }
}

#[derive(Debug)]
pub enum BlockRenderer {
    INT,
    FLOAT,
    STR,
    CHECKBOX,
}

#[derive(Debug)]
pub struct Block {
    id: String,
    label: String,
    renderer: BlockRenderer,
    disabled: bool,
    visible: bool,
    readonly: bool,
    required: bool,
    default_answer: BlockAnswer,
    answer: BlockAnswer,
}

#[derive(Debug)]
pub struct BlockBuilder {
    id: Option<String>,
    label: Option<String>,
    renderer: Option<BlockRenderer>,
    disabled: Option<bool>,
    visible: Option<bool>,
    readonly: Option<bool>,
    required: Option<bool>,
    default_answer: Option<BlockAnswer>,
    answer: Option<BlockAnswer>,
}

impl Default for BlockBuilder {
    fn default() -> Self {
        BlockBuilder {
            id: None,
            label: None,
            renderer: None,
            disabled: Some(false),
            visible: Some(true),
            readonly: Some(false),
            required: Some(false),
            default_answer: None,
            answer: None,
        }
    }
}

impl BlockBuilder {
    pub fn new() -> Self {
        BlockBuilder {
            ..Default::default()
        }
    }

    pub fn set_id(mut self, value: String) -> BlockBuilder {
        self.id = Some(value);
        self
    }

    pub fn set_label(mut self, value: String) -> BlockBuilder {
        self.label = Some(value);
        self
    }

    pub fn set_renderer(mut self, value: BlockRenderer) -> BlockBuilder {
        self.renderer = Some(value);
        self
    }

    pub fn set_disabled(mut self, value: bool) -> BlockBuilder {
        self.disabled = Some(value);
        self
    }

    pub fn set_visible(mut self, value: bool) -> BlockBuilder {
        self.visible = Some(value);
        self
    }

    pub fn set_readonly(mut self, value: bool) -> BlockBuilder {
        self.readonly = Some(value);
        self
    }

    pub fn set_required(mut self, value: bool) -> BlockBuilder {
        self.required = Some(value);
        self
    }

    pub fn build(self) -> Block {
        Block {
            id: self.id.unwrap(),
            label: self.label.unwrap(),
            renderer: self.renderer.unwrap(),
            disabled: self.disabled.unwrap(),
            visible: self.visible.unwrap(),
            readonly: self.readonly.unwrap(),
            required: self.required.unwrap(),
            default_answer: BlockAnswer::NONE,
            answer: BlockAnswer::NONE,
        }
    }
}

pub trait Question: std::fmt::Debug {

    fn node_id(&self, ctx: &MotorContext) -> String {
        let prefix = if self.parent() == "" {
            ctx.name.clone()
        } else {
            format!("{}.{}", ctx.name.clone(), self.parent())
        };

        format!("{}.{}", prefix, self.name())
    }

    fn name(&self) -> String;

    fn label(&self) -> String;

    fn renderer(&self) -> BlockRenderer;

    fn parent(&self) -> String {
        String::from("")
    }

    fn disabled(&self) -> bool {
        false
    }

    fn visible(&self) -> bool {
        true
    }

    fn readonly(&self) -> bool {
        false
    }

    fn required(&self) -> bool {
        false
    }

    fn default_answer(&self) -> BlockAnswer {
        BlockAnswer::NONE
    }

    fn answer(&self, ctx: &MotorContext) -> BlockAnswer {
        if let Some(answer) = ctx.answers.borrow().get(&self.node_id(ctx)) {
            if self.answer_validator().valid(answer.clone()) {
                return answer.clone()
            }
        }
        self.default_answer()
    }

    fn answer_validator(&self) -> Box<dyn BlockAnswerValidator> {
        Box::new(NoneValidator {})
    }

    fn dependencies(&self) -> &Vec<RcRefCellQuestion>;

    fn block_builder(&self, ctx: &MotorContext) -> BlockBuilder {
        BlockBuilder {
            id: Some(self.node_id(&ctx)),
            label: Some(self.label()),
            renderer: Some(self.renderer()),
            disabled: Some(self.disabled()),
            visible: Some(self.visible()),
            readonly: Some(self.readonly()),
            required: Some(self.required()),
            default_answer: Some(self.default_answer()),
            answer: Some(self.answer(&ctx)),
        }
    }

    fn process(&self, ctx: &MotorContext) {
        let answer = self.answer(&ctx);
    }
}

pub type RcRefCellAnswers = Rc<RefCell<HashMap<String, BlockAnswer>>>;
pub type RcRefCellQuestion = Rc<RefCell<dyn Question>>;
type QuestionGraph = DiGraph<RcRefCellQuestion, u32>;

#[derive(Debug, Clone)]
pub struct NodeMetadata {
    graph_index: NodeIndex,
    node: RcRefCellQuestion,
}

#[derive(Debug)]
pub struct MotorContext {
    name: String,
    deps_by_name: HashMap<String, NodeMetadata>,
    answers: RcRefCellAnswers,
}

#[derive(Default, Debug)]
pub struct Motor {
    name: String,
    nodes_metadata: HashMap<String, NodeMetadata>,
    sorted_nodes: Vec<RcRefCellQuestion>,
    dependencies: Vec<RcRefCellQuestion>,
    digraph: QuestionGraph,
    answers: RcRefCellAnswers,
}

impl Motor {
    pub fn new(name: String) -> Self {
        Motor {
            name,
            ..Default::default()
        }
    }

    pub fn push_dependency(&mut self, node: RcRefCellQuestion) {
        self.dependencies.push(node)
    }

    pub fn node_id(&self, node_ref: &RefCell<dyn Question>) -> String {
        let node = node_ref.borrow();
        let prefix = if node.parent() == "" {
            self.name.clone()
        } else {
            format!("{}.{}", self.name.clone(), node.parent())
        };

        format!("{}.{}", prefix, node.name())
    }

    pub fn build_graph(
        &self,
    ) -> (
        QuestionGraph,
        Vec<RcRefCellQuestion>,
        HashMap<String, NodeMetadata>,
    ) {
        let mut graph: QuestionGraph = DiGraph::new();
        let mut nodes_metadata: HashMap<String, NodeMetadata> = HashMap::new();

        // add all nodes
        for node in &self.dependencies {
            let node_index = graph.add_node(node.clone());
            nodes_metadata.insert(
                self.node_id(node),
                NodeMetadata {
                    graph_index: node_index.clone(),
                    node: node.clone(),
                },
            );
        }

        // create edges
        for node in &self.dependencies {
            let node_id = self.node_id(node);
            for dep_node in node.borrow().dependencies() {
                let dep_node_id = self.node_id(dep_node);
                let a = nodes_metadata.get(&node_id).unwrap().graph_index;
                let b = nodes_metadata.get(&dep_node_id).unwrap().graph_index;
                graph.add_edge(a, b, 0);
            }
        }

        let mut nodes_metadata_by_index: HashMap<NodeIndex, NodeMetadata> = HashMap::new();
        for (k, v) in &nodes_metadata {
            nodes_metadata_by_index.insert(v.graph_index.clone(), v.clone());
        }

        let sorted_nodes: Vec<RcRefCellQuestion> = match toposort(&graph, None) {
            Ok(nodes) => nodes
                .into_iter()
                .rev()
                .map(|x| nodes_metadata_by_index.get(&x).unwrap().node.clone())
                .collect(),
            Err(e) => {
                panic!("Graph contains a cycle: {:?}", e);
            }
        };

        (graph, sorted_nodes, nodes_metadata)
    }

    pub fn run(&mut self, answers: HashMap<String, BlockAnswer>) {
        let (graph, sorted_nodes, nodes_metadata) = self.build_graph();
        self.digraph = graph;
        self.sorted_nodes = sorted_nodes;
        self.nodes_metadata = nodes_metadata;
        self.answers = Rc::new(RefCell::new(answers));

        let mut ctx_by_node:HashMap<String, MotorContext> = HashMap::new();

        for node in &self.sorted_nodes {

            let deps_by_name: HashMap<String, NodeMetadata> = node.borrow()
                .dependencies()
                .into_iter()
                .map(|dep| {
                    let dep_name = dep.borrow().name();
                    let dep_metadata = self.nodes_metadata.get(&self.node_id(dep)).unwrap().clone();
                    (dep_name, dep_metadata)
                })
                .collect();

            let ctx = MotorContext {
                name: self.name.clone(),
                deps_by_name: deps_by_name,
                answers: self.answers.clone()
            };

            ctx_by_node.insert(self.node_id(node), ctx);
        }

        for node in &self.sorted_nodes {
            if let Some(ctx) = ctx_by_node.get(&self.node_id(node)) {
                node.borrow().process(ctx);
            }
        }


        for node in &self.sorted_nodes {
            if let Some(ctx) = ctx_by_node.get(&self.node_id(node)) {
                let block = node.borrow().block_builder(ctx).build();
                println!("{:#?}", block);
            }
        }
    }
}
