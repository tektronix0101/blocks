import camelCase from 'lodash/camelCase.js'
import {Block} from './block.js'

class Queue {
    constructor() {
        this.items = []
    }

    enqueue(value) {
        this.items.push(value)
    }

    dequeue() {
        return this.items.shift()
    }

    isEmpty() {
        return this.items.length === 0
    }
}

class DiGraph {
    constructor() {
        this.adjList = new Map()
    }

    addVertex(node) {
        this.adjList.set(node, [])
    }

    addEdge(aNode, bNode) {
        this.adjList.get(aNode).push(bNode)
    }

    *bfs(startingNode) {
        const visited = new Set()
        const queue = new Queue()
        queue.enqueue(startingNode)
        visited.add(startingNode)

        while(!queue.isEmpty()) {
            const node = queue.dequeue()
            yield node
            for (const childNode of this.adjList.get(node)) {
                if (!visited.has(childNode)) {
                    queue.enqueue(childNode)
                    visited.add(childNode)
                }
            }
        }
    }

    *dfs(startingNode, visited = new Set()) {
        visited.add(startingNode)
        yield startingNode
        for (const childNode of this.adjList.get(startingNode)) {
            if (!visited.has(childNode)) {
                yield* this.dfs(childNode, visited)
            }
        }
    }

    dfsTopSortHelper(node, n, visited, results) {
        visited.add(node)
        for (const neighbor of this.adjList.get(node)) {
            if (!visited.has(neighbor)) {
                n = this.dfsTopSortHelper(neighbor, n, visited, results);
            }
        }
        results.set(node, n)
        return n - 1
    }

    dfsTopSort() {
        const vertices = Array.from(this.adjList.keys())
        const visited = new Set()
        const results = new Map()
        let n = vertices.length - 1
        for (const node of vertices) {
            if (!visited.has(node)) {
                n = this.dfsTopSortHelper(node, n, visited, results)
            }
        }

        // make sure we don't have cycles
        for (const node of vertices) {
            for (const neighbor of this.adjList.get(node)) {
                const first = results.has(node) ? results.get(node) : 0
                const second = results.has(neighbor) ? results.get(neighbor) : 0
                if (first > second) {
                    throw new Error('Cycle exist')
                }
            }
        }

        return Array.from(results)
            .sort((a, b) => a[1] - b[1])
            .map(result => result[0])
    }
}

export class Motor {
    get blocks () {
        return []
    }

    _printGraph() {
        for (const node of this.graph.adjList.keys()) {
            let path = ''
            for (const childNode of this.graph.adjList.get(node)) {
                path += childNode.id + ' '
            }
            console.log(node.id + ' -> ' + path)
        }
    }

    _buildGraph() {
        this.graph = new DiGraph()
        this.blockClassToBlock = new Map()
        this.motorBlocksQueue = new Queue()

        const addGraphVertex = (blocks) => {
            for (const blockClass of new Set(blocks)) {
                if (Block.isPrototypeOf(blockClass)) {
                    const block = new blockClass()
                    block.motorName = this.constructor.name
                    this.graph.addVertex(block)
                    this.blockClassToBlock.set(blockClass, block)
                } else if (MotorBlock.isPrototypeOf(blockClass)) {
                    this.motorBlocksQueue.enqueue(blockClass)
                }
            }
        }

        const createVariants = (motorBlockClass) => {
            const motorBlock = new motorBlockClass()
            const sourceToVariantCls = new Map()
            for (const blockClass of new Set(motorBlock.blocks)) {
                if (this.blockClassToBlock.has(blockClass)) {
                    throw Error('Block already defined ' + blockClass.name)
                }
                const variantName = `${motorBlockClass.name}${blockClass.name}`
                const classes = {[variantName]: class extends blockClass {}}
                sourceToVariantCls.set(blockClass, classes[variantName])
            }

            for (const [sourceCls, variantCls] of sourceToVariantCls.entries()) {
                const block = new sourceCls()
                const newDeps = {}
                for (const [key, depBlockClass] of Object.entries(block.deps)) {
                    newDeps[key] = sourceToVariantCls.get(depBlockClass)
                }

                const variantName = `${motorBlockClass.name}_${sourceCls.name}`
                const classes = {
                    [variantName]: class extends sourceCls {
                        deps = newDeps
                    }
                }

                const variantCls2 = classes[variantName]
                Object.setPrototypeOf(variantCls, variantCls2)
            }

            return Array.from(sourceToVariantCls.values())
        }

        addGraphVertex(this.blocks)

        while (!this.motorBlocksQueue.isEmpty()) {
            const blocks = createVariants(this.motorBlocksQueue.dequeue())
            addGraphVertex(blocks)
        }

        for (const block of this.graph.adjList.keys()) {
            for (const depBlockClass of Object.values(block.getDeps())) {
                const depBlock = this.blockClassToBlock.get(depBlockClass)
                this.graph.addEdge(depBlock, block)
            }
        }

        this.sortedBlocks = this.graph.dfsTopSort()
        for (const block of this.sortedBlocks) {
            for (const [key, blockClass] of Object.entries(block.getDeps())) {
                block[camelCase(key)] = this.blockClassToBlock.get(blockClass)
            }
        }
    }

    run(answers = {}) {
        this._buildGraph()

        // load inbound answers
        for (const block of this.sortedBlocks) {
            block.answer =  answers[block.id]
        }

        return {
            blocks: this.sortedBlocks.map(block => block.toJSON())
        }
    }
}


export class MotorBlock {
    get blocks () {
        return []
    }
}
