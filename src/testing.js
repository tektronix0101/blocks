import {ShortText, LongText, Integer, FieldSet} from './block.js'
import {Motor, MotorBlock} from './motor.js'


class G0 extends FieldSet {
    label = 'Parent Group'
}

class G1 extends FieldSet {
    label = 'Group 1'
    parent = G0
}

class G2 extends FieldSet {
    label = 'Group 2'
    parent = G0
}

class Q1 extends ShortText {
    label = 'Question 1'
    parent = G1
}

class Q2 extends LongText {
    deps = {Q1}
    parent = G1

    get label() {
        return `Question 2 - ${this.q1.answer}`
    }
}

class Q3 extends Integer {
    deps = {Q2}
    label = 'Question 3'
    parent = G2
}

class Q4 extends Integer {
    deps = {Q2}
    label = 'Question 4'
    parent = G2
}

class Q5 extends Integer {
    deps = {Q1}
    label = 'Question 5'
}

class Q6 extends Integer {
    label = 'Question 6'
}

class Q7 extends Integer {
    label = 'Question 7'
    deps = {Q6}
}

class TestMotorBlock extends MotorBlock {
    blocks = [Q6, Q7]
}

class Phase1 extends TestMotorBlock {}

class Phase2 extends TestMotorBlock {}


class TestMotor extends Motor {
    blocks = [
        G0,
        G1,
        Q1,
        Q2,
        G2,
        Q3,
        Q4,
        Q5,
        Phase1,
        Phase2
    ]
}


const motor = new TestMotor()
const results = motor.run({'TestMotor.G0.G1.Q1': 'some text'})
// motor._printGraph()
console.log(JSON.stringify(results, null, 2))
