import Ajv from 'ajv'

const ajv = new Ajv()

export class Block {

    /* Block spec properties */

    parent = undefined
    motorName = undefined

    get id() {
        return `${this.parent?.id || this.motorName}.${this.constructor.name}`
    }

    get label() {
        return ''
    }

    get renderer() {
        throw new Error('Please define renderer')
    }

    get rendererProps() {
        return {}
    }

    get disabled() {
        return false
    }

    get visible () {
        return true
    }

    get required () {
        return false
    }

    get defaultAnswer() {
        return undefined
    }

    get answer() {
        return this.hasOwnProperty('_answer') ? this._answer : this.defaultAnswer
    }

    set answer(value) {
        this._answer = this.validateAnswer(value)
    }

    get schema() {
        return {}
    }

    /* Addition behavior */

    validateAnswer(value) {
        const validate = ajv.compile(this.schema)
        if (validate(value)) {
            return value
        }
        return this.defaultAnswer
    }


    toJSON() {
        return {
            id: this.id,
            parent: this.parent?.id || '',
            label: this.label,
            renderer: this.renderer,
            rendererProps: this.rendererProps,
            disabled: this.disabled,
            visible: this.visible,
            required: this.required,
            answer: this.answer
        }
    }

    get deps() {
        return {}
    }

    getDeps() {
        if (Block.isPrototypeOf(this.parent)) {
            return {...this.deps, parent: this.parent}
        }
        return this.deps
    }

}


class Text extends Block {
    get maxLength() {
        return undefined
    }

    get minLength() {
        return 0
    }

    get rendererProps() {
        return {
            maxLength: this.maxLength,
            minLength: this.minLength
        }
    }

    get schema() {
        return {
            type: 'string',
            minLength: this.minLength,
            maxLength: this.maxLength
        }
    }
}


export class ShortText extends Text {
    get renderer() {
       return 'short-text'
    }
}


export class LongText extends Text {
    get renderer() {
       return 'long-text'
    }
}


export class Integer extends Block {
    get renderer() {
       return 'integer'
    }

    get minimum () {
        return undefined
    }

    get maximum () {
        return undefined
    }

    get step () {
        return 1
    }

    get rendererProps() {
        return {
            minimum: this.minimum,
            maximum: this.maximum,
            step: this.step
        }
    }

    get schema() {
        return {
            type: 'integer',
            minimum: this.minimum,
            maximum: this.maximum,
        }
    }
}


export class FieldSet extends Block {
    get renderer() {
        return 'fieldset'
    }
}
